# Конвертер валют на базе API ЦБ РФ #

Задание к модулю 39 курса Go. Предлагалось взять готовый код и 
настроить CI. Но это не спортивно и потому код был написан свой)

Приложение в отдельном потоке периодически получает курсы валют из
API ЦБ РФ и позволяет через веб-интерфейс узнавать курсы валют друг
к другу. Конвертация (предсказуемо) через рубли.

Интервал обновления по умолчанию и listen-сокет указываются в файле
cmd/config.go.

По умолчанию 600 и 0.0.0.0:8080 соответственно.

Интервал обновления можно переопределить через переменную окружения
UPDATE_INTERVAL

Исходный код тут: https://gitlab.com/moose_kazan/cbrcurrencyconverterweb

Докер-образ тут: https://hub.docker.com/r/bulvinkl/cbr-currency-converter

Запуск:

`docker run -it --rm  -p 8080:8080 \
    docker.io/bulvinkl/cbr-currency-converter:main`

Получить свежую версию:

`docker pull docker.io/bulvinkl/cbr-currency-converter:main`

Запустить с другим интервалом обновления данных:
`docker run -it --rm  -p 8080:8080 \
    -e UPDATE_INTERVAL=3600 \
    docker.io/bulvinkl/cbr-currency-converter:main`
